CREATE TABLE clientes
( 
id_cliente int IDENTITY(1,1) PRIMARY KEY,
nombres varchar(20),
apellidos varchar(20),
identificacion int UNIQUE
);

CREATE TABLE vehiculo
( 
id_vehiculo int IDENTITY(1,1) PRIMARY KEY,
marca varchar(20),
placa varchar(20) UNIQUE,
);

CREATE TABLE vehiculos_clientes
( 
id_vehiculo_cliente int IDENTITY(1,1) PRIMARY KEY,
id_cliente int not null,
id_vehiculo int not null,
CONSTRAINT fk_cliente FOREIGN KEY (id_cliente) REFERENCES clientes (id_Cliente),
CONSTRAINT fk_vehiculos FOREIGN KEY (id_vehiculo) REFERENCES vehiculo (id_vehiculo)
);

CREATE TABLE registro
( 
id_registro int IDENTITY(1,1) PRIMARY KEY,
id_vehiculo_cliente int not null,
fecha_ingreso date not null,
fecha_salida date,
precio int,
CONSTRAINT fk_cliente FOREIGN KEY (id_vehiculo_cliente) REFERENCES vehiculos_clientes (id_vehiculo_cliente)
);
