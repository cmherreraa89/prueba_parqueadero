import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  myForm: FormGroup;
  registro: any = [];
  constructor(
    public fb: FormBuilder,
  ) {
    this.myForm = this.fb.group({
      vehiculo: ['', Validators.required],
      fecha_inicio: ['', Validators.required],
      fecha_fin: ['', [Validators.required, Validators.email]],
       cliente: ['', [Validators.required, Validators.minLength(7), Validators.maxLength(20)]],
      // phone: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(20)]],
      // education : ['', Validators.required]
    });
  }

  saveData() {

  }

}
